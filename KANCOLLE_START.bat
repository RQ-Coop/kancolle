@ECHO OFF
REM KANCOLLE START BATCH

REM DOS窓を最小化状態で起動する為、再帰呼び出しを行う。
if not "%MADO%"=="CHILD" (
    set MADO=CHILD
    start /min cmd /c KANCOLLE_START.bat
    exit
)

ECHO;
ECHO カレントディレクトリを変更
cd .\logbook_x86

ECHO;
ECHO リモートリポジトリからプル
git pull > ..\Pull.log

ECHO;
ECHO 航海日誌を起動（launch_x86.batから流用）
SET JVM_OPT=-d32 -Xms16m -Xmx128m
START /wait javaw %JVM_OPT% -jar logbook.jar

ECHO;
ECHO 航海日誌を終了したのでコミット後、リモートリポジトリへプッシュ
git commit -a -m "実績反映" > ..\Commit.log
git push origin master > ..\Push.log

REM デバッグモードの場合はポーズ
if "%MODE%"=="DEBUG" (
    ECHO;
    ECHO 結果確認用のポーズ
    PAUSE
)

ECHO;
ECHO あばよ
exit
